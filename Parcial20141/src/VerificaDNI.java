/*
 * Comprobar este algoritmo en: https://cel.reniec.gob.pe/valreg/valreg.do
 * Este codigo presenta varias lineas comentadas que sirven para obtener la
 * letra de verificacion del DNI, pero en los DNIs actuales se utiliza el
 * numero es vez de la letra.
 * Para entender mejor algunas partes de codigo, es necesario saber/recordar
 * que: int = char - 48
 * Ejemplo: 4 = '4' - 48
 */

/**
 *
 * @author Heberq
 */
public class VerificaDNI {

    final static int[] peso = {3, 2, 7, 6, 5, 4, 3, 2};
//    final static char[] letras = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'};
    final static char[] digitos = {'7', '8', '9', '0', '1', '1', '2', '3', '4', '5', '6'};

    public static Boolean validaDni(String dni) {
        // comprobamos que el dni ingresado sea un número
        try {
            Integer.parseInt(dni);
        } catch (NumberFormatException e) {
            return false;
        }
        // si el numero es de 8 digitos devuelve verdad, sino falso
        return (dni.length() <= 8);
    }

    private static int indiceDni(String dni) {
        int suma = 0;
        for (int i = 0; i < dni.length(); i++) {
            suma = suma + (dni.charAt(i) - 48) * peso[i];
        }
        suma = suma % 11;
        return ((digitos.length - suma) - 1);
    }

    public static String digitoVerificacionDNI(String dni) {
        if (validaDni(dni)) {
            return "" + digitos[indiceDni(dni)];
        }
        return null;
    }

    public static String muestraDni(String dni) {
        if (validaDni(dni)) {
            return dni + " - " + digitoVerificacionDNI(dni);
        }
        return "ERROR: valor de DNI ingresado es inválido";
    }

//    public static String letraVerificacionDNI(String dni) {
//        if (validaDni(dni)) {
//            return String.valueOf(letras[indiceDni(dni)]);
//        }
//        return null;
//    }
//
//    public static String muestraDigitoLetra(String dni){
//        if(validaDni(dni)){
//            return dni + " - "+digitoVerificacionDNI(dni)+"("+letraVerificacionDNI(dni)+")";
//        }
//        return "valor de DNI ingresado es inválido";
//    }
}
