# LÉEME #

### ¿Qué encuentro en este repositorio? ###

* Encuentras ejercicios resueltos del curso de LPOO
* Puedes descargar lo que desees

### ¿Cómo pruebo el código? ###

* Debes descargar los proyectos, los podrás abrir directamente con NetBeans
* Si deseas abrirlos en otros IDEs, puedes copiar los archivos +.java de la carpeta src de cada proyecto, luego los copias
* en las carpetas donde guardas las clases con el IDE que usas, de ser el caso, debe añadir la línea de package en cada 
* archivo para evitar errores.

### Correcciones y comentarios ###
* heberqc@gmail.com