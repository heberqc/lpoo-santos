/*
 * Este codigo muestra en consola, mediante la funcion estatica 'graficar', un
 * texto que contenga las letras que se han programado en 'graficarLetras', en
 * el ejemplo solo estan definida las letras f, i, s,-, 2, 0, 1 y 4.
 * Todo se mostrara en un recuadro.
 * Aparte de la cadena que se mostrara, tambien se pide la dimension de las
 * letras, o sea, el tamaño de las letras que se mostraran.
 */

/**
 *
 * @author Heberq
 */
public class TextoRecuadro {
    
    public static char MARCA= '#';

    public static void graficar(String cadena, int dimension) {
        String texto = cadena.trim().toUpperCase();
        char[][] matriz = new char[dimension + 4][texto.length() * (dimension + 1) + 2];
        recuadro(matriz, dimension, texto.length());
        for (int i = 0; i < texto.length(); i++) {
            graficaLetras(matriz, 2, 2 + (dimension + 1) * i, dimension, texto.charAt(i));
        }
        mostrar(matriz, dimension, texto.length());
    }

    static void mostrar(char[][] matriz, int dimension, int cant) {
        for (int f = 0; f < dimension + 4; f++) {
            for (int c = 0; c < cant * (dimension + 1) + 2; c++) {
                System.out.print("" + matriz[f][c]);
            }
            System.out.println("");
        }
    }

    static void recuadro(char[][] matriz, int dimension, int cant) {
        for (int f = 0; f < dimension + 4; f++) {
            for (int c = 0; c < cant * (dimension + 1) + 2; c++) {
                if (f == 0 || f == dimension + 3 || c == 0 || c == cant * (dimension + 1) + 1) {
                    matriz[f][c] = MARCA;
                } else {
                    matriz[f][c] = ' ';
                }
            }
        }
    }

    static void graficaLetras(char[][] matriz, int i, int j, int dim, char letra) {
        for (int f = 0; f < dim; f++) {
            for (int c = 0; c < dim; c++) {
                switch (letra) {
                    case 'F':
                        if (c == 0 || f == 0 || f == dim / 2) {
                            matriz[i + f][j + c] = MARCA;
                        }
                        break;
                    case 'I':
                        if (c == dim / 2) {
                            matriz[i + f][j + c] = MARCA;
                        }
                        break;
                    case 'S':
                        if (f == dim / 2 || f == 0 || f == dim - 1 || (c == 0 && f < dim / 2) || (c == dim - 1 && f > dim / 2)) {
                            matriz[i + f][j + c] = MARCA;
                        }
                        break;
                    case '-':
                        if (f == dim / 2 && !(c == 0 || c == dim - 1)) {
                            matriz[i + f][j + c] = MARCA;
                        }
                        break;
                    case '2':
                        if (f == dim / 2 || f == 0 || f == dim - 1 || (c == 0 && f > dim / 2) || (c == dim - 1 && f < dim / 2)) {
                            matriz[i + f][j + c] = MARCA;
                        }
                        break;
                    case '0':
                        if (f == 0 || f == dim - 1 || c == 0 || c == dim - 1) {
                            matriz[i + f][j + c] = MARCA;
                        }
                        break;
                    case '1':
                        if (c - 1 == dim / 2 || c == dim / 2 - f + 1 && c != 0) {
                            matriz[i + f][j + c] = MARCA;
                        }
                        break;
                    case '4':
                        if (c == dim - 1 || (c == 0 && f < dim / 2) || f == dim / 2) {
                            matriz[i + f][j + c] = MARCA;
                        }
                        break;
                }
            }
        }
    }
}
