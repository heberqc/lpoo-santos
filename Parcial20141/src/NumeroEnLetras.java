/*
 * Este codigo posee una sola funcion estatica que devuelve un numero de hasta
 * tres cifras, incluyendo el cero, en letras.
 * Recordar que: int = char - 48
 * Ejemplo: 4 = '4' - 48
 */

/**
 *
 * @author Heberq
 */

//Esto es una prueba

public class NumeroEnLetras {
    
    static final String[] menosDe30 = {"", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve", "diez", "once", "doce", "trece", "catorce", "quince", "dieciseis", "diecisiete", "dieciocho", "diecinueve", "veinte", "veintiuno", "veintidos", "veintitres", "veinticuatro", "veinticinco", "veintiseis", "veintisiete", "veintiocho", "veintinueve"};
    static final String[] unidades = {"", " y uno", " y dos", " y tres", " y cuatro", " y cinco", " y seis", " y siete", " y ocho", " y nueve"};
    static final String[] decenas = {"", "", "", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa"};
    static final String[] centenas = {"", "ciento ", "doscientos ", "trescientos ", "cuatrocientos ", "quinientos ", "seiscientos ", "setecientos ", "ochocientos ", "novecientos "};
    static final String[][] numero = {centenas, decenas, unidades};

    public static String enTexto(int num) {
        String cadena = String.valueOf(num);
        for (; cadena.length() < 3;) {
            cadena = "0" + cadena;
        }
        String salida = "";
        if(num==0){
            return "cero";
        }else if (cadena.charAt(0) == '1' && cadena.charAt(1) == '0' && cadena.charAt(2) == '0') {
            return "cien";
        } else if (cadena.charAt(1) < '3') {
            int indice = cadena.charAt(0) - 48;
            salida = numero[0][indice] + menosDe30[Integer.parseInt("" + cadena.charAt(1) + cadena.charAt(2))];
        } else if (cadena.charAt(1) >= '3' || (cadena.charAt(0) > '0' && cadena.charAt(1) == '0' && cadena.charAt(2) == '0')) {
            for (int i = 0; i < 3; i++) {
                int indice = cadena.charAt(i) - 48;
                salida = salida + numero[i][indice];
            }
        }
        return salida;
    }

}
