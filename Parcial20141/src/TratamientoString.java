/*
 * Esta clase tiene implementadas las funciones que piden:
 * 1.- La frecuencia de las vocales en el texto ingresado
 * 2.- Eliminar los espacios innecesarios, normalizar los espacios del texto
 * 3.- Mostrar la cadena original invertida
 */

/**
 *
 * @author Heberq
 */
public class TratamientoString {

    public final static char[] vocales = {'a', 'e', 'i', 'o', 'u'};

    public static String frecuenciaVocales(String texto) {
        String cadena = texto.toUpperCase().trim();
        int[] contador = {0, 0, 0, 0, 0};
        for (int i = 0; i < cadena.length(); i++) {
            switch (cadena.charAt(i)) {
                case 'A':
                    contador[0]++;
                    break;
                case 'E':
                    contador[1]++;
                    break;
                case 'I':
                    contador[2]++;
                    break;
                case 'O':
                    contador[3]++;
                    break;
                case 'U':
                    contador[4]++;
                    break;
                default:
                    break;
            }
        }
        return "FRECUENCIA DE LAS VOCALES" + "\n"
                + "=========================" + "\n"
                + "Texto: \"" + texto + "\"\n"
                + "A: " + contador[0] + "\n"
                + "E: " + contador[1] + "\n"
                + "I: " + contador[2] + "\n"
                + "O: " + contador[3] + "\n"
                + "U: " + contador[4] + "\n";
    }

    public static String eliminaEspacios(String texto) {
        int i = 0;
        String salida = "";
        Boolean espacio;
        for (;;) {
            try {
                espacio = false;
                while (texto.charAt(i) == ' ') {
                    i++;
                    espacio = true;
                }
                if (espacio && !salida.equals("")) {
                    salida = salida + " ";
                }
                while (texto.charAt(i) != ' ') {
                    salida = salida + texto.charAt(i);
                    i++;
                }
            } catch (StringIndexOutOfBoundsException e) {
                break;
            }
        }
        return salida;
    }

    public static String invertir(String texto) {
        String salida = "";
        for (int i = texto.length() - 1; i >= 0; i--) {
            salida = salida + texto.charAt(i);
        }
        return salida;
    }
}

