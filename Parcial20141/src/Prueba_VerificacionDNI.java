
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Prueba_VerificacionDNI {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("CODIGO DE VERIFICACION DE DNI");
        System.out.println("=============================");
        System.out.print("Ingrese DNI: ");
        String cadena = br.readLine();
        System.out.println("El DNI completo es: " + VerificaDNI.muestraDni(cadena));
    }

}
