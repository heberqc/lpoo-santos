
public class Prueba_TratamientoString {

    public static void main(String[] args) {
        String string = "        arriBA     PeRu     PERU     siempre      ArRIba    ";
        System.out.println(TratamientoString.frecuenciaVocales(string));
        string = TratamientoString.eliminaEspacios(string);
        System.out.println("Regulando los espacios : \"" + string + "\"");
        System.out.println("");
        System.out.println("Texto invertido :\"" + TratamientoString.invertir(string) + "\"");
        System.out.println("");
    }

}
